//
//  main.m
//  Details
//
//  Created by Kumaran Vijayan on 2014-07-07.
//  Copyright (c) 2014 Kumaran Vijayan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDPAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([TDPAppDelegate class]));
	}
}
