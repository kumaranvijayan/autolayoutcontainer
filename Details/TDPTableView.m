//  Created by Kumaran Vijayan on 2014-07-12.

#import "TDPTableView.h"

@implementation TDPTableView

- (CGSize)intrinsicContentSize
{
	CGSize size = self.contentSize;
	return size;
}

- (void)setContentSize:(CGSize)contentSize
{
	[super setContentSize:contentSize];
	[self invalidateIntrinsicContentSize];
}

@end
