//  Created by Kumaran Vijayan on 2014-07-07.

#import <UIKit/UIKit.h>

@interface TDPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
