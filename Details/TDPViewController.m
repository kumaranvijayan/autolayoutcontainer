//  Created by Kumaran Vijayan on 2014-07-07.

#import "TDPViewController.h"

@interface TDPViewController ()
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@end

@implementation TDPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setUpLabelView];
}

- (NSArray*)constraintsForContainedView:(UIView*)view
{
	NSDictionary *views = NSDictionaryOfVariableBindings(view);
	NSArray *horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
																			 options:0
																			 metrics:nil
																			   views:views];
	NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view(>=0)]|"
																		   options:0
																		   metrics:nil
																			 views:views];
	return [horizontalConstraints arrayByAddingObjectsFromArray:verticalConstraints];
}

- (void)removeContainedViews
{
	for (UIView *subview in self.containerView.subviews)
	{
		[subview removeFromSuperview];
	}
	
	for (UIViewController *childViewController in self.childViewControllers)
	{
		[childViewController removeFromParentViewController];
	}
}

- (void)setUpLabelView
{
	UILabel *label = [UILabel new];
	label.translatesAutoresizingMaskIntoConstraints = NO;
	label.numberOfLines = 0;
	label.preferredMaxLayoutWidth = 320.0;
	[label setText:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est."];
	[self.containerView addSubview:label];
	[self.containerView addConstraints:[self constraintsForContainedView:label]];
}

- (void)setUpTableView
{
	UIViewController *tableView = [self.storyboard instantiateViewControllerWithIdentifier:@"TableView"];
	[tableView.view setTranslatesAutoresizingMaskIntoConstraints:NO];
	[self addChildViewController:tableView];
	[self.containerView addSubview:tableView.view];
	[self.containerView addConstraints:[self constraintsForContainedView:tableView.view]];
}

#pragma mark - Interface Actions

- (IBAction)tabSwitch:(UISegmentedControl*)sender
{
	[self removeContainedViews];
	if (sender.selectedSegmentIndex == 0)
	{
		[self setUpLabelView];
	}
	else
	{
		[self setUpTableView];
	}
}

@end
